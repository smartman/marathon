<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\AthleteTime;
use App\Events\NewTimeEvent;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Event;

class LogicTest extends TestCase {

    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHome() {
        $response = $this->call('GET', '/');
        $this->assertEquals(200, $response->status());
    }

    public function testHistoryStatus() {
        $response = $this->call('GET', '/history');
        $this->assertEquals(200, $response->status());
    }

    public function testListenerSuccess() {
        factory("App\AthleteTime")->create();
        $this->expectsEvents('App\Events\NewTimeEvent');
        $appKey = env("SOURCE_KEY");
        $response = $this->call('GET', "/api/v1/listen?key=$appKey&athlete_number=101&time=100.100&location=finish_start");
        $this->assertEquals(200, $response->status());
    }

    public function testListenerJson() {
        factory("App\AthleteTime")->create();
        $this->withoutEvents();
        $appKey = env("SOURCE_KEY");
        $this->json('GET', "/api/v1/listen?key=$appKey&athlete_number=101&time=100.100&location=finish_start")
                ->seeJson(["status" => "OK"])
                ->seeStatusCode(200);
    }

    public function testHistoryJson() {
        factory("App\AthleteTime")->create();
        $lastId = AthleteTime::max("id");
        $this->json('GET', "/history")
                ->seeJson(["lastId" => $lastId])
                ->seeStatusCode(200);
    }

    public function testListenerWrongKey() {
        $this->withoutEvents();
        $appKey = env("SOURCE_KEY") . "x";
        $response = $this->call('GET', "/api/v1/listen?key=$appKey&athlete_number=101&time=100.100&location=finish_start");
        $this->assertEquals(401, $response->status());
    }

    public function testListenerWrongAthlete() {
        $this->withoutEvents();
        $appKey = env("SOURCE_KEY");
        $response = $this->call('GET', "/api/v1/listen?key=$appKey&athlete_number=abc&time=100.100&location=finish_start");
        $this->assertEquals(422, $response->status());
    }

    public function testListenerWrongTime() {
        $this->withoutEvents();
        $appKey = env("SOURCE_KEY");
        $response = $this->call('GET', "/api/v1/listen?key=$appKey&athlete_number=101&time=100.1234&location=finish_start");
        $this->assertEquals(422, $response->status());
    }

    public function testListenerWrongLocation() {
        $this->withoutEvents();
        $appKey = env("SOURCE_KEY");
        $response = $this->call('GET', "/api/v1/listen?key=$appKey&athlete_number=101&time=100.100&location=start");
        $this->assertEquals(422, $response->status());
    }

}
