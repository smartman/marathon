let historyUrl = "http://localhost:8000/history";
let pusherKey = '51feafa338a9d45ff91a';

let pusher = new Pusher(pusherKey, {
    cluster: 'eu',
    encrypted: true
});

let channel = pusher.subscribe('times');

let helloApp = angular.module("MarathonResults", ['ui.bootstrap']);
helloApp.controller("ResultsCtrl", function ($scope, $filter, $window, $http) {
    $scope.missingWarning = false;
    $scope.athletes = [];
    let lastId = 0;

    $scope.refreshState = function () {
        console.log("Refreshing state");
        $http.get(historyUrl)
                .then(function (response) {
                    console.log(response.data);
                    $scope.lastId = response.data.lastId;
                    $scope.athletes = $filter('orderBy')(response.data.times, 'eventTime', true);
                });
    };
    $scope.refreshState();

    $scope.listenMessages = function (data) {
        let idDiff = data.id - $scope.lastId;
        if (idDiff > 1) {
            $scope.refreshState();
            return;
        }
        $scope.lastId = data.id;
        let newEntry = {
            name: data.athlete_name,
            number: data.athlete_number,
            time: '',
            eventTime: parseFloat(data.time)
        };

        if (data.location === "finish") {
            newEntry.time = data.time;
            angular.forEach($scope.athletes, function (value, key) {
                if (value.number === newEntry.number) {
                    $scope.athletes.splice(key, 1);
                }
            });
        }
        var value = newEntry;
        $scope.athletes.push(value);
        $scope.athletes = $filter('orderBy')($scope.athletes, 'eventTime', true);
        $scope.missingWarning = false;
        $scope.$apply();
    };

    let pusherState = pusher.connection.state;

    channel.bind('new-time', $scope.listenMessages);


    $window.onblur = function () {
        $scope.missingWarning = true;
        $scope.$apply();
        console.log("Disconnecting from server ...");
        channel.unbind('new-time');
    };

    $window.onfocus = function () {
        console.log("Reconnecting to server");
        channel.bind('new-time', $scope.listenMessages);
    };

});
