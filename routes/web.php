<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

$app->get('/api/v1/listen', [
    'middleware' => 'valid_source',
    'uses' => 'ListenController@listen'
]);

$app->get('/', 'HomeController@getHome');
$app->get('/history', 'HomeController@getHistory');
