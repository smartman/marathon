<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AthleteTimesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("athlete_times", function($t) {
            $t->engine = "InnoDB";
            $t->increments("id");
            $t->decimal("time", 10, 3);
            $t->enum("location", ["finish_start", "finish"]);
            $t->integer("athlete_number")->unsigned();
            $t->foreign("athlete_number")->references("number")->on("athletes");
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop("athlete_times");
    }

}
