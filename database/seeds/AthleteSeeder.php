<?php

use Illuminate\Database\Seeder;

class AthleteSeeder extends Seeder {

    /**
     * Creates 10 000 Athletes. Will take some time to complete.
     */
    public function run() {
        echo 'Will take around 3s to complete';
        for ($i = 100; $i < 1101; $i++) {
            factory(App\Athlete::class)->create(["number" => $i]);
        }
    }

}
