<?php

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | Here you may define all of your model factories. Model factories give
  | you a convenient way to create models for testing and seeding your
  | database. Just tell the factory how a default model should look.
  |
 */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});

$factory->define('App\Athlete', function ($faker) {
    return [
        'name' => $faker->name,
        'number' => 101
    ];
});

$factory->define('App\AthleteTime', function ($faker) {
    $athlete = App\Athlete::find(101);
    if ($athlete == null) {
        $athlete = factory('App\Athlete')->create();
    }
    return [
        'athlete_number' => $athlete->number,
        'time' => "100.100",
        'location' => "finish"
    ];
});
