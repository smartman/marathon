<!DOCTYPE html>
<html ng-app="MarathonResults">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Marathon results</title>

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/table.css">
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    </head>
    <body ng-controller="ResultsCtrl" class="ng-cloak">
        <div class="text-center">
            <h1>Marathon results</h1>
        </div>    
        <div class="container">
            <div class="alert alert-warning"  ng-show="missingWarning">There could be some times missing while you were away from page, please refresh the page to get current state</div>
            <div class="jumbotron text-center" ng-show="!athletes.length">
                Waiting for finishers, they are coming ...<br><br><img src="/img/waiting.gif" alt="Waiting for finishers">
            </div>
        </div>
        <table ng-show="athletes.length" class="table-fill">
            <thead>
                <tr>
                    <th class="text-left">Number</th>
                    <th class="text-left">Name</th>
                    <th class="text-left">Time</th>
                </tr>
            </thead>
            <tbody class="table-hover">
                <tr ng-repeat="athlete in athletes">
                    <td class="text-left">{{athlete.number}}</td>
                    <td class="text-left">{{athlete.name}}</td>
                    <td class="text-left">{{athlete.time}}</td>
                </tr>   
            </tbody>
        </table>

        <script src="https://js.pusher.com/4.1/pusher.min.js"></script>        

        <!--<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>-->
        <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>-->
        <script src="/js/marathon-results.js" type="text/javascript"></script>        
        <script src="/js/ui-bootstrap-tpls-2.5.0.min.js" type="text/javascript"></script>   
    </body>
</html>
