# Marathon time registry sample app

## User guide

  - run `composer update` to download all prerequisites
  - create Mysql database and enter its credentials to .env
  - run `php artisan migrate`, to create database
  - run `php artisan db:seed` to create 1 000 sample Athletes to database, it will take around 3s
  - start PHP builtin server with command `php -S localhost:8000 -t public`
  - change Pusher key and history URL in marathon-results.js if needed.
  - run below bash script to start getting results. Change apphost value if needed
  - to clear the database from events run `php artisan migrate:refresh` and php `artisan db:seed`.
  - run `phpunit` to execute tests 

## Time measuring test scripts

Use below bash script to test the system

```bash
startdate=`date +%s`;
startdate=`expr $startdate - 1000`;
apphost="http://localhost:8000";
for i in {100..10100}
do
   currentdate=`date +%s`;
   timediff=`expr $currentdate - $startdate`.$(((RANDOM % 1000)))
   curl -w "\n" "$apphost/api/v1/listen?key=bEUd58QyQR&athlete_number=$i&time=$timediff&location=finish_start"
   sleep 1
   timediff=`expr $currentdate + 1 - $startdate`.$(((RANDOM % 1000)))
   curl -w "\n" "$apphost/api/v1/listen?key=bEUd58QyQR&athlete_number=$i&time=$timediff&location=finish"
   sleep 1
done
```

## Finish corridor entering dummy client ##
First the website shows athlete 101 above 100 as 101 arrived later and later times are on top. After 3 seconds order will be reversed and athlete 101 is on top as 100 passed it on finish corridor.

```bash
apphost="http://localhost:8000";
curl -w "\n" "$apphost/api/v1/listen?key=bEUd58QyQR&athlete_number=101&time=100.100&location=finish_start";curl -w "\n" "$apphost/api/v1/listen?key=bEUd58QyQR&athlete_number=100&time=100.000&location=finish_start";
sleep 3;
curl -w "\n" "$apphost/api/v1/listen?key=bEUd58QyQR&athlete_number=100&time=103.400&location=finish";curl -w "\n" "$apphost/api/v1/listen?key=bEUd58QyQR&athlete_number=101&time=103.500&location=finish";
```