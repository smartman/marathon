<?php

namespace App\Http\Middleware;

use Closure;

class ValidSourceMiddleware {

    /**
     * Makes sure that only valid sources can send the data
     */
    public function handle($request, Closure $next) {
        $key = env("SOURCE_KEY");
        if ($request->key !== $key) {
            return response('Unauthorized.', 401);
        }
        return $next($request);
    }

}
