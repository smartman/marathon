<?php

namespace App\Http\Controllers;

use App\Events\NewTimeEvent;
use Illuminate\Http\Request;
use App\AthleteTime;

class ListenController extends Controller {

    public function __construct() {
        //
    }

    public function listen(Request $request, AthleteTime $athleteTime) {
        $this->validate($request, [
            'athlete_number' => 'required|exists:athletes,number',
            'time' => ["required", "regex:/(?<![\d.])(\d{1,9}|\d{0,9}\.\d{1,3})?(?![\d.])/"],
            'location' => 'required|in:finish_start,finish'
        ]);
        $athleteTime->athlete_number = $request->athlete_number;
        $athleteTime->time = $request->time;
        $athleteTime->location = $request->location;
        $athleteTime->save();
        info("Athlete time $athleteTime->id saved: $athleteTime->athlete_number, $athleteTime->time, $athleteTime->location");
        event(new NewTimeEvent($athleteTime));
        return response()->json(["status" => "OK", "time_id" => $athleteTime->id]);
    }

}
