<?php

namespace App\Http\Controllers;

use App\AthleteTime;

class HomeController extends Controller {

    public function __construct() {
        //
    }

    public function getHome() {
        return view("home");
    }

    //get current state of the athlete results 
    public function getHistory() {
        $finishedTimes = AthleteTime::where("location", "finish")->with("athlete")->get();
        $finishedAthleteNumbers = $finishedTimes->pluck("athlete_number");
        $unFinishedTimes = AthleteTime::where("location", "finish_start")->whereNotIn("athlete_number", $finishedAthleteNumbers)->with("athlete")->get();
        $allTimes = $finishedTimes->merge($unFinishedTimes);
        $result = [];
        $lastId = 0;
        foreach ($allTimes as $time) {
            $result["times"][] = [
                "id" => $time->id,
                "name" => $time->athlete->name,
                "number" => $time->athlete_number,
                "time" => ($time->location === "finish") ? $time->time : "",
                "eventTime" => floatval($time->time),
            ];
            $lastId = max([$lastId, $time->id]);
        }
        $result["lastId"] = $lastId;
        return response()->json($result);
    }

}
