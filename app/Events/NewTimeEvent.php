<?php

namespace App\Events;

use App\AthleteTime;

class NewTimeEvent extends Event {

    public $time;

    public function __construct(AthleteTime $time) {
        $this->time = $time;
    }

}
