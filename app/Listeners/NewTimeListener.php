<?php

namespace App\Listeners;

use Pusher\Pusher;
use App\Events\NewTimeEvent;

class NewTimeListener {

    private $pusher;

    public function __construct() {
        $options = array(
            'cluster' => 'eu',
            'encrypted' => true
        );
        $pusher = new Pusher(env('PUSHER_AUTH_KEY'), env('PUSHER_SECRET'), env('PUSHER_APP_ID'), $options);
        $this->pusher = $pusher;
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent  $event
     * @return void
     */
    public function handle(NewTimeEvent $event) {
        $athlete = $event->time->athlete;
        $data = [
            "id" => $event->time->id,
            "athlete_name" => $athlete->name,
            "athlete_number" => $athlete->number,
            "time" => $event->time->time,
            "location" => $event->time->location
        ];
        $this->pusher->trigger('times', 'new-time', $data);
    }

}
