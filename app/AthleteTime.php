<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AthleteTime extends Model {

    public function athlete() {
        return $this->belongsTo("App\Athlete", "athlete_number", "number");
    }

}
